# Rackspace RMS Demo
This project is the answer for the "Dev Exercise" for Landon Jurgens. Submitted to Rackspace on May 24, 2017.

## Setup and Use
The solution was solved using [Python 3.5.3](https://www.python.org/downloads/release/python-353/) on Ubuntu Linux 17.04, and testing was limited to this environment. The specific version string from the Python interceptor is:

```Python 3.5.3 (default, Jan 19 2017, 14:11:04) [GCC 6.3.0 20170118] on linux``` 

### Installation
The installation process is very basic for this example. There are no dependencies outside the Python standard library; therefore there is no typical installation required other than having a Python interpreter.

1. Have a system with Python3 installed.
2. Clone or download the repository.
3. Navigate to the indexer subdirectory.
4. Execute the command you would like with the `python` prepended; see usage below. (Note: If Python2 is the default for your system, you may need to use `python3` instead. Check the version with `python --version` if unsure.)

### Testing

To run the unit tests for the question, navigate to the sub-folder "indexer", and execute the `indexerTests.py` file. Example output:

    $ python indexerTests.py
    Ran 25 tests in 0.039s

    OK

## Explanation

### Stated Question
>*Required functions*
>
>* Write logic that takes a blob of text as a parameter and tokenizes this blob into words. Words are delimited by any character other than a-z, A-Z, or 0-9.
>
>* Write logic to track all unique words encountered and the number of times each was encountered. Words should be matched in a case-insensitive manner. This should return the top 10 words (and their counts).
>
>* Provide some documentation for the code you wrote in each of the previous steps.
>
>* You must test your code. Make sure you include some brief documentation on how to run the tests. Any collection of plain text files can be used as input, and we suggest you try out some free plain text books from http://www.gutenberg.org/
>
>*All of the following steps are optional. You may complete any number of them, or none at all.*
>
>* Write a command-line interface for your indexer that takes the file names of text blobs as arguments, and then prints the top 10 words across all files to standard output.
>
>* Use source control to help you develop this software. If possible, we'd like you to post this on a public platform like GitHub or Bitbucket and then send us a link.
>
>* Ensure that you can run your code in places other than your own development environment, and provide installation/deployment instructions. Provide documentation that walks the user through using your application.
>
>* Extend your application to execute concurrently. You may choose to support a fixed, configurable number of workers or to allow changing the number of workers dynamically.
>
>* Extend your application to be distributed, such that workers can run on separate machines from each other. Hint: you may leverage existing open source technologies to accomplish this.

### Solution 1
The first solution is implemented in the `indexer.py` file. This implementation aims to capture the minimum specifications of the stated question above.

>* Write logic that takes a blob of text as a parameter and tokenizes this blob into words. Words are delimited by any character other than a-z, A-Z, or 0-9.
>
>* Write logic to track all unique words encountered and the number of times each was encountered. Words should be matched in a case-insensitive manner. This should return the top 10 words (and their counts).
>
>* Provide some documentation for the code you wrote in each of the previous steps.
>
>* You must test your code. Make sure you include some brief documentation on how to run the tests. Any collection of plain text files can be used as input, and we suggest you try out some free plain text books from http://www.gutenberg.org/

The application essentially wraps Python's [Counter](https://docs.python.org/3.5/library/collections.html#collections.Counter) object with some basic IO functionality. The Counter object contains all the logic for the ask of taking all the parsed words and counting how frequently each one appears. From Counter, we're using the `most_common()` function to derive the results. The word parsing is completed by using Python's Regex parser to split all the words along the regex's `\W+` meta character. This splits along the [a-zA-Z0-9] character set. This was abstracted using a constant at the top of the file.

Here is the command line interface usage
```bash
$ python indexer.py -h
usage: indexer.py [-h] [--count COUNT] data

positional arguments:
  data                  The inputted string to be parse

optional arguments:
  -h, --help            show this help message and exit
  --count COUNT, -c COUNT
                        The number of top results

```

#### Results
Here are the results from Python's [PEP20](https://www.python.org/dev/peps/pep-0020/) _The Zen of Python_:

```bash
$ python indexer.py "$(cat zen.txt)"
Top results
Results
Word     - Count
IS       - 10
BETTER   - 8
THAN     - 8
THE      - 5
TO       - 5
BE       - 3
NEVER    - 3
ONE      - 3
ALTHOUGH - 3
IDEA     - 3
```

### Solution 2
The second solution aims to address some optional steps outlines above:

>* Write a command-line interface for your indexer that takes the file names of text blobs as arguments, and then prints the top 10 words across all files to standard output.
>
>* Extend your application to execute concurrently. You may choose to support a fixed, configurable number of workers or to allow changing the number of workers dynamically.

This implementation was done in the filename `fileIndexer.py`. This program leveraged Python's [multiprocessing](https://docs.python.org/3.5/library/multiprocessing.html) library to be able to separate the tasks based on file names into multiple processes. Each subprocess opens the file, processes the file, and returns the result to the root Python process. The root process collects the results and prints them to standard out once everything is complete. The advantage of this approach is to be able to simply wrap the original function with a file open command and process each one independently. By using the subprocess command, there is a separate Python process for each task. The scheduling is a simple async task where each worker takes a task when free and returns the result when it finishes, and no inter-process communication is needed. Additionally since each subprocess has its own Python interpreter, it's able to exercise multiple CPU cores for any CPU intensive tasks.

Here is the command line interface usage
```bash
$ python fileIndexer.py -h
usage: fileIndexer.py [-h] [--process PROCESS] [--count COUNT]
                      infiles [infiles ...]

positional arguments:
  infiles

optional arguments:
  -h, --help            show this help message and exit
  --process PROCESS, -p PROCESS
                        The number of processes to spawn
  --count COUNT, -c COUNT
                        The number of top results

```

#### Results
Here are the results from Python's [PEP20](https://www.python.org/dev/peps/pep-0020/) _The Zen of Python_:

```bash
$ python fileIndexer.py zen.txt zen1.txt 
Top results for: zen.txt
Word     - Count
IS       - 10
BETTER   - 8
THAN     - 8
THE      - 5
TO       - 5
ONE      - 3
IT       - 3
IDEA     - 3
NEVER    - 3
ALTHOUGH - 3

Top results for: zen1.txt
Word     - Count
IS       - 10
BETTER   - 8
THAN     - 8
THE      - 5
TO       - 5
ONE      - 3
IT       - 3
IDEA     - 3
NEVER    - 3
ALTHOUGH - 3


```

### Additional Answers
To address some of the additional questions not explicitly related to code, some other tasks were completed in addition to the Python coding.

#### Source Control
>Use source control to help you develop this software. If possible, we'd like you to post this on a public platform like GitHub or Bitbucket and then send us a link.

All source code was developed from the start in git and pushed up to this Bitbucket repo. By simply following industry best practices, this question was addressed.

#### Portability
>Ensure that you can run your code in places other than your own development environment, and provide installation/deployment instructions. Provide documentation that walks the user through using your application.

By using a standard language version and leveraging core Python libraries, this solution set was able to limit the amount of setup to simple possession of the core Python files themselves. No other outside libraries are required. As a result, it works on any system that has Python3 installed.