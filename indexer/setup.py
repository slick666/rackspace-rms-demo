from setuptools import setup, find_packages

setup(
    name="indexer",
    version="0.1",
    packages=find_packages(),
    scripts=['indexer.py', 'fileIndexer.py'],
)
