"""
Indexer: take in text from standard input and counts unique words
"""

# Standard Library Imports
import argparse
import re
from collections import Counter
import sys

SPLIT_REGEX = r'\W+'
DEFAULT_COUNT = 10


def check_positive_int(value):
    """
    simple function to test f the object passed in is an integer and greater than 0
    :param value: object to be tests
    :return: integer value
    """
    try:
        int_value = int(value)
        assert int_value > 0
    except:
        raise argparse.ArgumentTypeError("%s is an invalid value only positive numbers " % value)

    return int_value


def parse_arguments(args):
    """
    Simple sub-function that handles all argument parsing and checking
    :return: parser argument with count and data fields 
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "data",
        default="",
        help="The inputted string to be parse")
    parser.add_argument(
        "--count", "-c",
        type=check_positive_int,
        default=DEFAULT_COUNT,
        help="The number of top results")

    args = parser.parse_args(args)

    return args


def top_words(input_data=None, count=10):
    """
    Simple function to take a string of data, parse that string into words and
    return the top N number of results

    :param input_data: string data to be parsed
    :param count: The number of most common results
    :return: Tuples of the top elements with key and count
    """

    return Counter([
        word.upper()
        for word in re.split(SPLIT_REGEX, input_data)
    ]).most_common(count)


def main(args):
    """
    Main loop of execution.
    :param args: object with attributes for infiles, and count
    :return: None
    """

    print("Top results")
    results = top_words(input_data=args.data, count=args.count)
    max_width = max(len(word) for word, _ in results)
    format_string = "{word:<%s} - {count}" % max_width

    print("Results")
    print(format_string.format(word="Word", count="Count"))

    for word, count in results:
        print(format_string.format(word=word, count=count))


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
