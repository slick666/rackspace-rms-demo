"""
Implements the indexer on a series of file.
"""

# Standard Library Imports
import argparse
from multiprocessing import cpu_count, Pool
import sys

# Application Imports
from indexer import top_words, check_positive_int

SPLIT_REGEX = r'\W+'
DEFAULT_COUNT = 10
DEFAULT_PROCESSES = cpu_count() * 2


def parse_arguments(args):
    """
    Simple sub-function that handles all argument parsing and checking
    :return: parser argument with count and data fields 
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'infiles',
        nargs='+',
        default=sys.stdin
    )
    parser.add_argument(
        "--process", '-p',
        type=check_positive_int,
        default=DEFAULT_PROCESSES,
        help="The number of processes to spawn"
    )
    parser.add_argument(
        "--count", "-c",
        type=check_positive_int,
        default=10,
        help="The number of top results")

    return parser.parse_args(args)


def top_words_file(filename, count):
    """
    This function is designed to be executed inside of a multiprocessing pool worker taking the filename and count 
    argument to execute the top_words function on the whole file. 
    :param filename: filename to be read
    :param count: the number of top results to return
    :return: Tuples of the top elements with key and count
    """

    with open(filename, 'r') as infile:
        results = top_words(infile.read(), count)

    return results


def main(args):
    """
    Main loop of execution.
    :param args: object with attributes for infiles, count, and process
    :return: None
    """
    with Pool(processes=args.process) as pool:
        results = pool.starmap(
            func=top_words_file,
            iterable=[(file_object, args.count) for file_object in args.infiles],
            chunksize=1
        )

    for filename, result in zip(args.infiles, results):
        print("Top results for: %s" % filename)

        max_width = max(len(word) for word, _ in result)
        format_string = "{word:<%s} - {count}" % max_width
        print(format_string.format(word="Word", count="Count"))

        for word, count in result:
            print(format_string.format(word=word, count=count))

        print()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
