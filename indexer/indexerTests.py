"""Unit Tests for indexer.py and fileIndexer.py"""

import unittest

from indexer import top_words, DEFAULT_COUNT
from indexer import parse_arguments as indexer_parse_arguments
from fileIndexer import parse_arguments as file_parse_arguments
from fileIndexer import DEFAULT_COUNT as FILE_DEFAULT_COUNT
from fileIndexer import DEFAULT_PROCESSES as FILE_DEFAULT_PROCESSES

top_words_datasets = [
    ['every word is unique',
     [('WORD', 1),
      ('UNIQUE', 1),
      ('EVERY', 1),
      ('IS', 1)]
     ],
    ['There are some repeats in this string, only some',
     [('SOME', 2),
      ('THERE', 1),
      ('ARE', 1),
      ('REPEATS', 1),
      ('IN', 1),
      ('THIS', 1),
      ('STRING', 1),
      ('ONLY', 1),
      ]
     ]
]


class TestTopWords(unittest.TestCase):
    def test_top_words(self):
        for input_data, expected_output in top_words_datasets:
            self.assertEqual(
                sorted(top_words(input_data, None)),
                sorted(expected_output)
            )


class TestIndexerArgs(unittest.TestCase):

    def test_attrs(self):
        args = indexer_parse_arguments(["block of string"])
        self.assertTrue(hasattr(args, 'data'))
        self.assertTrue(hasattr(args, 'count'))

    def test_default_input(self):
        string_input = "block of string"

        args = indexer_parse_arguments([string_input])
        self.assertEqual(args.data, string_input)
        self.assertEqual(args.count, DEFAULT_COUNT)

    def test_count_input(self):
        string_input = "block of string"
        count_input = 7

        args = indexer_parse_arguments([string_input, '-c', str(count_input)])
        self.assertEqual(args.data, string_input)
        self.assertEqual(args.count, count_input)

    def test_no_input(self):
        with self.assertRaises(SystemExit):
            args = indexer_parse_arguments([])

    def test_missing_text(self):
        with self.assertRaises(SystemExit):
            args = indexer_parse_arguments(['-c', '7'])

    def test_zero_count(self):
        string_input = "block of string"
        count_input = 0

        with self.assertRaises(SystemExit):
            args = indexer_parse_arguments([string_input, '-c', str(count_input)])

    def test_neg_count(self):
        string_input = "block of string"
        count_input = -7

        with self.assertRaises(SystemExit):
            args = indexer_parse_arguments([string_input, '-c', str(count_input)])

    def test_non_int_count(self):
        string_input = "block of string"
        count_input = "three"

        with self.assertRaises(SystemExit):
            args = indexer_parse_arguments([string_input, '-c', count_input])


class TestFileIndexerArgs(unittest.TestCase):

    def test_attrs(self):
        args = file_parse_arguments(["filename1"])
        self.assertTrue(hasattr(args, 'infiles'))
        self.assertTrue(hasattr(args, 'count'))
        self.assertTrue(hasattr(args, 'process'))

    def test_default_input(self):
        filenames = ["filename1"]

        args = file_parse_arguments(filenames)
        self.assertEqual(args.infiles, filenames)
        self.assertEqual(args.count, FILE_DEFAULT_COUNT)
        self.assertEqual(args.process, FILE_DEFAULT_PROCESSES)

    def test_count_input(self):
        filenames = ["filename1"]
        count_input = 7

        args = file_parse_arguments(filenames + ['-c', str(count_input)])
        self.assertEqual(args.infiles, filenames)
        self.assertEqual(args.count, count_input)
        self.assertEqual(args.process, FILE_DEFAULT_PROCESSES)

    def test_process_input(self):
        filenames = ["filename1"]
        process_input = 7

        args = file_parse_arguments(filenames + ['-p', str(process_input)])
        self.assertEqual(args.infiles, filenames)
        self.assertEqual(args.count, FILE_DEFAULT_COUNT)
        self.assertEqual(args.process, process_input)

    def test_combined_input(self):
        filenames = ["filename1"]
        count_input = 11
        process_input = 7

        args = file_parse_arguments(filenames + ['-c', str(count_input), '-p', str(process_input)])
        self.assertEqual(args.infiles, filenames)
        self.assertEqual(args.count, count_input)
        self.assertEqual(args.process, process_input)

    def test_no_input(self):
        with self.assertRaises(SystemExit):
            args = file_parse_arguments([])

    def test_missing_text(self):
        with self.assertRaises(SystemExit):
            args = file_parse_arguments(['-c', '7'])

    def test_zero_count(self):
        string_input = "filename1"
        count_input = 0

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-c', str(count_input)])

    def test_zero_process(self):
        string_input = "filename1"
        process_input = 0

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-p', str(process_input)])

    def test_zero_combined(self):
        string_input = "filename1"
        count_input = 0
        process_input = 0

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-p', str(process_input), '-c', str(count_input)])

    def test_neg_count(self):
        string_input = "filename1"
        count_input = -7

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-c', str(count_input)])

    def test_neg_process(self):
        string_input = "filename1"
        process_input = -7

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-p', str(process_input)])

    def test_neg_combined(self):
        string_input = "filename1"
        count_input = -7
        process_input = -11

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-p', str(process_input), '-c', str(count_input)])

    def test_non_int_count(self):
        string_input = "filename1"
        count_input = "three"

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-c', count_input])

    def test_non_int_process(self):
        string_input = "filename1"
        process_input = "three"

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input, '-p', process_input])

    def test_non_int_combined(self):
        string_input = "filename1"
        count_input = "three"
        process_input = "seven"

        with self.assertRaises(SystemExit):
            args = file_parse_arguments([string_input,  '-p', process_input, '-c', count_input])

if __name__ == '__main__':
    unittest.main(buffer=False)
